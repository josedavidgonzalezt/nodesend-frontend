module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['https://node-send-servidor.herokuapp.com'],
  },
  env: {
    backendURL: 'https://node-send-servidor.herokuapp.com',
    frontendURL: 'https://node-send-client-psi.vercel.app'
  },
}
