import {
  AUTHENTICATED_USER,
  AUTH_USER,
  CREATE_ACCOUNT,
  CREATE_ACCOUNT_FAIL,
  CLEAN_ALERT,
  AUTH_USER_FAIL,
  LOGOUT,
} from "../types";

const authReducer = (state, action) => {
  switch (action.type) {
    case CREATE_ACCOUNT:
    case CREATE_ACCOUNT_FAIL:
    case AUTH_USER_FAIL:
      return {
        ...state,
        message: action.payload,
      };

    case CLEAN_ALERT:
      return {
        ...state,
        message: action.payload,
      };
  
    case AUTH_USER:
      localStorage.setItem('rns_token', action.payload)
      return {
        ...state,
        token: action.payload,
        autenticado: true
      };
    case AUTHENTICATED_USER:
    return {
      ...state,
      user: action.payload,
      autenticado: true,
    };
    case LOGOUT:
      localStorage.removeItem('rns_token')
      return {
        ...state,
        token: null,
        autenticado: null,
        user: null,
      };

    default:
      return state;
  }
};

export default authReducer;
