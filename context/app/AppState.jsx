import { useReducer } from "react";
import axiosClient from "../../config/axios";
import {
  SHOW_ALERT,
  CLEAN_ALERT,
  UPLOAD_FILES,
  UPLOAD_FILES_SUCCESS,
  UPLOAD_FILES_ERROR,
  CREATE_LINK_SUCCESS,
  CREATE_LINK_ERROR,
  CLEAN_STATE,
  ADD_PASSWORD,
  ADD_DOWNLOADS
} from "../../types";
import AppContext from "./appContext";
import appReducer from "./appReducer";

const AppState = ({children}) => {
  const initialState = {
    message_file: null,
    name_file: null,
    original_name: null,
    loading: false,
    downloads: 1,
    password: null,
    author: null,
    url: null,
  }

  const [state, dispatch = ({ type, payload}) => null] = useReducer(appReducer, initialState)

  // show alert
  const showAlert = (msg) => {
    dispatch({
      type: SHOW_ALERT,
      payload: msg
    })

    setTimeout(() => {
      dispatch({
        type: CLEAN_ALERT,
      })
    }, 3000);
  }

  // upload files
  const uploadFiles = async(formData, originalName) => {
    dispatch({
      type: UPLOAD_FILES,
    })

    try {
      
    const { data } = await axiosClient.post('/api/files', formData)
    dispatch({
      type: UPLOAD_FILES_SUCCESS,
      payload: {
        name: data.file,
        originalName
      }
    })
    } catch (error) {
      dispatch({
        type: UPLOAD_FILES_ERROR,
        payload: error.response.data.msg
      })
    }
  }

  const createLink = async () => {
    const dataLink = {
      name: state.name_file,
      original_name: state.original_name,
      downloads: state.downloads,
      author: state.author,
      url: state.url,
      password: state.password
    }
    try {
      const response = await axiosClient.post('/api/links', dataLink)
      console.log(response.data)
      dispatch({
        type: CREATE_LINK_SUCCESS,
        payload: response.data
      })
    } catch (error) {
      dispatch({
        type: CREATE_LINK_ERROR,
        payload: error.response.data.errores[0].msg
      })
      console.log(error.response.data.errores[0].msg)
    }
  }

  const cleanState = () => {
    dispatch({
      type: CLEAN_STATE
    })
  }

  const addPassword = (password) => {
    dispatch({
      type: ADD_PASSWORD,
      payload: password
    })
  }

  const addDownloads = (downloads) =>  {
    dispatch({
      type: ADD_DOWNLOADS,
      payload: downloads,
    })
  } 

  
  return (
    <AppContext.Provider
      value={{
        message_file: state.message_file,
        name_file: state.name_file,
        original_name: state.original_name,
        downloads: state.downloads,
        password: state.password,
        author: state.author,
        url: state.url,
        showAlert,
        uploadFiles,
        createLink,
        cleanState,
        addPassword,
        addDownloads
      }}
    >
      {children}
    </AppContext.Provider>
  );
}
 
export default AppState;
