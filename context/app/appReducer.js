import {
  SHOW_ALERT,
  CLEAN_ALERT,
  UPLOAD_FILES,
  UPLOAD_FILES_SUCCESS,
  UPLOAD_FILES_ERROR,
  CREATE_LINK_SUCCESS,
  CREATE_LINK_ERROR,
  CLEAN_STATE,
  ADD_PASSWORD,
  ADD_DOWNLOADS
} from "../../types";

const appReducer = (state, action= { payload, type}) =>{
  switch (action.type) {
    case SHOW_ALERT:
      return {
        ...state,
        message_file: action.payload,
      }
    case CLEAN_ALERT:
      return {
        ...state,
        message_file: null,
      }
  
    case UPLOAD_FILES:
      return {
        ...state,
        loading: true,
      }
    case UPLOAD_FILES_SUCCESS:
      return {
        ...state,
        name_file: action.payload.name,
        original_name: action.payload.originalName,
        loading: false,
      }
    case UPLOAD_FILES_ERROR:
    case CREATE_LINK_ERROR:
      return {
        ...state,
        message_file: action.payload,
        loading: false,
      }
    case CREATE_LINK_SUCCESS:
      return {
        ...state,
        message_file: action.payload.msg,
        url: action.payload.url,
      }
  
    case CLEAN_STATE:
      return {
        ...state,
        message_file: null,
        name_file: null,
        original_name: null,
        loading: false,
        downloads: 1,
        password: null,
        author: null,
        url: null,
      }
    case ADD_PASSWORD:
      return {
        ...state,
        password: action.payload,
      }
    case ADD_DOWNLOADS:
      return {
        ...state,
        downloads: action.payload,
      }
  
    default:
      return state;
  }
}

export default appReducer;