import { createContext } from "react";

const AppContext = createContext({
  message_file: null,
  name_file: null,
  original_name: null,
  loading: false,
  downloads: 1,
  password: null,
  author: null,
  url: null,
  addPassword: (password) => null,
  addDownloads: (cant) => null,
  createLink: () => null,
  cleanState: () => null,
  uploadFiles: () => null,
  showAlert: () => null,
});

export default AppContext