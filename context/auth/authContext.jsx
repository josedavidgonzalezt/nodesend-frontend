import { createContext } from "react";

const AuthContext = createContext({
  token: '',
  autenticado: '',
  user: '',
  message: '',
  loading: '',
  authenticatedUser: () => null,
  registerUser: (user) => null,
  authUser: (user) => null,
  logout: () => null,
})

export default AuthContext;