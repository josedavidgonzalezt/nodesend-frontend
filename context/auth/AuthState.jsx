import { useReducer, useEffect } from "react";
import axiosClient from "../../config/axios";
import tokenAuth from "../../config/tokenAuth";
import {
  AUTHENTICATED_USER,
  AUTH_USER,
  CREATE_ACCOUNT,
  CREATE_ACCOUNT_FAIL,
  CLEAN_ALERT,
  AUTH_USER_FAIL,
  LOGOUT,
} from "../../types";
import authReducer from "../authReducer";
import AuthContext from "./authContext";

const AuthState = ({ children }) => {
  const initialState = {
    token: typeof window !== 'undefined' ? localStorage.getItem('rns_token') : '',
    autenticado: null,
    user: null,
    message: null,
  };

  const [state, dispatch] = useReducer(authReducer, initialState);
  const { token } = state;

  useEffect(() => {
    if(token) authenticatedUser()
  }, [token])

  const registerUser = async (user) => {
    try {
      const response = await axiosClient.post("/api/users", user);
      dispatch({
        type: CREATE_ACCOUNT,
        payload: response.data.msg,
      });
      console.log(response);
    } catch (error) {
      dispatch({
        type: CREATE_ACCOUNT_FAIL,
        payload: error.response.data.msg,
      });
      console.log(error);
    }

    // clean alert after 3 seconds
    cleanAlert()
  };

  const authUser = async (user) => {
    try {
      const response = await axiosClient.post("/api/auth", user);
      dispatch({
        type: AUTH_USER,
        payload: response.data.token,
      });
    } catch (error) {
      console.log(error.response.data.msg);
      dispatch({
        type: AUTH_USER_FAIL,
        payload: error.response.data.msg,
      });
    }
    await cleanAlert()
  };


  const cleanAlert = () => {
    setTimeout(() => {
      dispatch({
        type: CLEAN_ALERT,
        payload: null,
      });
    }, 3000);

  }

   // user auth
   const authenticatedUser = async() => {
    
    const token = localStorage.getItem('rns_token')
    if(token) {
      tokenAuth(token)
    }
    try {
      const { data } = await axiosClient.get("/api/auth")
      dispatch({
        type: AUTHENTICATED_USER,
        payload: data.user,
      });
    } catch (error) {
      console.log(error.response.data.msg);
      dispatch({
        type: AUTH_USER_FAIL,
        payload: error.response.data.msg,
      });
    }
  };

  // cerrar sesion
  const logout = () => {
    dispatch({
      type: LOGOUT,
    })
  }

  return (
    <AuthContext.Provider
      value={{
        token: state.token,
        autenticado: state.autenticado,
        user: state.user,
        message: state.message,
        loading: state.loading,
        authenticatedUser,
        registerUser,
        authUser,
        logout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthState;
