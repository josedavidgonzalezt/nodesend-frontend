import Image from 'next/image'
import image from '../public/logo.svg'
import Link from 'next/link'
import { useContext } from 'react'
import AuthContext from '../context/auth/authContext'
import AppContext from '../context/app/appContext'
import { useRouter } from 'next/router'

const Header = () => {
  const authContext = useContext(AuthContext);
  const { user, logout } = authContext;

  const appContext = useContext(AppContext);
  const { cleanState  } = appContext

  const router = useRouter()

  const redirectToHome = () => {
    router.push('/')
    cleanState()
  }

  return (
    <header className="py-8 flex flex-col md:flex-row items-center justify-between">
     <Image
       className="w-20 mb-8 md:mb-0 cursor-pointer"
       src={image}
       alt="logo"
       width="200"
       height="100"
       onClick={() => redirectToHome()}
     />
      <div>
        {user
          ?
          <div className="flex items-center">
            <p className="mr-2">hola {user.nombre}</p>
            <button
              type="button"
              className="bg-black px-5 py-3 rounded-lg text-white font-bold uppercase"
              onClick={() => logout()}
            >Cerrar Sesion</button>
          </div>
          :
          <>
            <Link href="/login">
              <a className="bg-red-500 px-5 py-3 rounded-lg text-white font-bold uppercase mr-2">Iniciar Sesion</a>
            </Link>
            <Link href="/crearcuenta">
              <a className="bg-black px-5 py-3 rounded-lg text-white font-bold uppercase">Crear Cuenta</a>
            </Link>
          </>
        }
        
      </div>
    </header>
  )
}
 
export default Header;