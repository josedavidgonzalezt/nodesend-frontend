import { useContext, useState } from "react";
import Alerta from "../../components/Alerta";
import axiosClient from "../../config/axios";
import AppContext from "../../context/app/appContext";

export const getServerSideProps = async ({ params }) => {
  const { link } = params;
  const response = await axiosClient.get("/api/links/" + link);
  console.log(response);
  return {
    props: {
      link: response.data,
    },
  };
};

export const getServerSidePaths = async () => {
  const links = await axiosClient.get("api/links");

  return {
    paths: links.data.links.map((link) => ({
      params: {
        link: link.url,
      },
    })),
    fallback: false,
  };
};

const Link = ({ link }) => {
  const appContext = useContext(AppContext)
  const { showAlert, message_file } = appContext
  const [hasPassword, setHasPassword] = useState(link.password);
  const [password, setPassword] = useState("")
  console.log(link);

  const validatePassword = async(e) => {
    e.preventDefault()
    if (!password) return
    try {
      const response = await axiosClient.post(`api/links/${link.link}`, {password})
      setHasPassword(response.data.password)

    } catch (error) {
      showAlert(error.response.data.msg)
    }
  }
  return (
    <div>
      {hasPassword ? (
        <>
          { message_file && <Alerta msg={message_file} />}
          <p className="text-center">
            Este enlace esta protegido por un password, colocalo a continuación:
          </p>
          <div className="flex justify-center mt-5">
            <div className="w-full max-w-lg">
              <form
                className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
                onSubmit={(e) => validatePassword(e)}
              >
                <div className="mb-4">
                  <label
                    className="block text-black text-sm font-bold mb-2"
                    htmlFor="password"
                  >
                    Contraseña
                  </label>
                  <input
                    className="shadow appereance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
                <input
                  type="submit"
                  className="bg-red-500 hover:bg-gray-900 w-full p-2 text-white uppercase font-bold"
                  value="Validar Password"
                />
              </form>
            </div>
          </div>
        </>
      ) : (
        <>
          <h1 className="text-4xl text-center text-gray-700">
            Descarga tu archivo
          </h1>
          <div className="flex items-center justify-center mt-10">
            <a
              href={`${process.env.backendURL}/api/files/${link.file}`}
              download
              className="bg-red-500 text-center px-10 py-3 rounded uppercase font-bold text-white cursor-pointer"
            >
              Aqui
            </a>
          </div>
        </>
      )}
    </div>
  );
};

export default Link;
