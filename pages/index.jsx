import Link from "next/link";
import { useContext } from "react";
import Alerta from "../components/Alerta";
import DropZone from "../components/DropZone";
import AppContext from "../context/app/appContext";

export default function Home() {

  // app Context
  const appContext = useContext(AppContext);
  const { message_file, url } = appContext;

 

  const linkDoesNotExist = (
    <>
    {message_file && <Alerta msg={message_file} />}
    <div className="lg:flex md:shadow-lg p-5 bg-white rounded-lg py-10 ">
      <DropZone/>
      <div className="md:flex-1 mb-3 mx-2 mt-16 lg:mt-0">
        <h2 className="text-4xl font-sans font-bold text-gray-800 my-4">Compartir Archivos de forma sencilla y privada</h2>
        <p className="text-lg leading-loose">
          <span className="text-red-500 font-bold">ReactNodeSend </span>
          te permite compartir archivos con cifrado de extremo a extremo y un archivos que es eliminado despues de ser descargado. Asi que puedes mantener lo que cmpartes en privado y asegurarte de que tus cosas no permanezcan en linea para siempre.
        </p>
        <Link href="/crearcuenta">
          <a className="text-red-500 font-bold text-lg hover:text-red-700">Crea una cuenta para mayores beneficios</a>
        </Link>
      </div>
    </div>
    </>
  )

  const existUrl = (
    <>
      <p className="text-center text-2xl" >
        <span className="font-bold text-red-700 text-3xl uppercase">Tu url es:</span>  <Link href={`${process.env.frontendURL}/links/${url}`}>{`${process.env.frontendURL}/links/${url}`}</Link>
      </p>
      <button
        type="button"
        className="bg-red-500 hover:bg-gray-900 w-full p-2 text-white uppercase font-bold mt-10"
        value="Login"
        onClick={() => navigator.clipboard.writeText(`${process.env.frontendURL}/links/${url}`)}
        onCopy={(e) => console.log(e)}
      >Copiar Enlnace</button>
    </>
  )

  return (
    <div className="md:w-4/5 xl:w-3/5 mx-auto mb-32">
      {url
        ? existUrl
        : linkDoesNotExist
      }
    </div>
  )
}
