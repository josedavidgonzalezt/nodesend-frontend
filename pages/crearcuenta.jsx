import { useFormik } from "formik";
import { useContext } from "react";
import * as Yup from 'yup'
import Alerta from "../components/Alerta";
import AuthContext from "../context/auth/authContext";

const CrearCuenta = () => {

  const authContext = useContext(AuthContext);
  const  { registerUser, message } = authContext

  // validation and form
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      password: '',
    },
    validationSchema: Yup.object({
      name: Yup.string().required('El nombre es Obligatorio'),
      email: Yup.string().email('El email no es valido').required("El email es obligatorio"),
      password: Yup.string().required("Introduzca su password").min(6, "El password debe contener 6 caractreres"),
    }), 
    onSubmit: user => {
      registerUser(user)
    }
  })

  return (
    <div className="md:w-4/5 xl:w3/5 mx-auto mb-32">
      <h2 className="text-4xl font-sans font-bold text-gray-800 text-center my4">Crear Cuenta</h2>
      { message && <Alerta/> }
      <div className="flex justify-center mt-5">
        <div className="w-full max-w-lg">
          <form
            className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
            onSubmit={formik.handleSubmit}
          >
            <div className="mb-4">
              <label
                className="block text-black text-sm font-bold mb-2"
                htmlFor="name"
              >Nombre</label>
              <input
                className="shadow appereance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="name"
                name="name"
                type="text"
                value={formik.values.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                placeholder="Nombre de usuario"
              />
              {formik.touched.name && formik.errors.name
                && <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4 ">
                  <p className="font-bold">Error</p>
                  <p>{formik.errors.name}</p>
                </div>
              }
            </div>
  
            <div className="mb-4">
              <label
                className="block text-black text-sm font-bold mb-2"
                htmlFor="email"
              >Email</label>
              <input
                id="email"
                name="email"
                type="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                placeholder="Email de usuario"
                className="shadow appereance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              />
              {formik.touched.email && formik.errors.email
                && <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4 ">
                  <p className="font-bold">Error</p>
                  <p>{formik.errors.email}</p>
                </div>
              }
            </div>
  
            <div className="mb-4">
              <label
                className="block text-black text-sm font-bold mb-2"
                htmlFor="password"
              >Contraseña</label>
              <input
                id="password"
                autoComplete="on"
                placeholder="Email de usuario"
                type="password"
                name="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                className="shadow appereance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              />
              {formik.touched.password && formik.errors.password
                && <div className="my-2 bg-gray-200 border-l-4 border-red-500 text-red-700 p-4 ">
                  <p className="font-bold">Error</p>
                  <p>{formik.errors.password}</p>
                </div>
              }
            </div>
            <input
              type="submit"
              className="bg-red-500 hover:bg-gray-900 w-full p-2 text-white uppercase font-bold"
              value="Crear Cuenta"
            />
          </form>
        </div>
      </div>
    </div>
  );
}
 
export default CrearCuenta;