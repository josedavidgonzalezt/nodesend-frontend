import Layout from '../components/Layout'
import '../styles/globals.css'
import 'tailwindcss/tailwind.css'
import AuthState from '../context/auth/AuthState'
import AppState from '../context/app/AppState'

function MyApp({ Component, pageProps }) {
  return (
    <AuthState>
      <AppState>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </AppState>
    </AuthState>
  )
}

export default MyApp
